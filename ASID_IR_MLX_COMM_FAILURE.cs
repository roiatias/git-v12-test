
using PressSdk.CodeGenerators.Abstract.Patterns.SeDefinitions;
using SE.ErrorsDefinitions.ErrorEvents.Enums;
using static SE.ErrorsDefinitions.ErrorEvents.Enums.ErrorEnumerations;

namespace SE.ErrorsDefinitions.ErrorEvents
{
    [ErrorEvent]
    [Description("No communication with aSID temperature sensor")]
    [Extension(nameof(ErrorEventExtension.EventHelper))]
    [Classification(nameof(ErrorEventClassification.AsidEvents))]
    [WhatToDo("Verify sensor wiring are not damaged")]
    [SuspectedCause("Sensor malfunction or damaged wiring 55555555555555555555555555555")]
    [EventTrigger("aSID temperature sensor communication has timed-out")]
    public class ASID_IR_MLX_COMM_FAILURE
    {
        [SensorIDParam]
        public SensorID Sensor { get; }

    }
}